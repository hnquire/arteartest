package com.example.arteartest.model

import com.google.gson.annotations.SerializedName

data class ASections (

	@SerializedName("containers") val containers : List<Containers>
)