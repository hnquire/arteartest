package com.example.arteartest.model

import com.google.gson.annotations.SerializedName

data class Media (

	@SerializedName("type") val type : String,
	@SerializedName("data") val data : Data
)