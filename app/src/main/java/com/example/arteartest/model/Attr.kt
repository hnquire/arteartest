package com.example.arteartest.model

import com.google.gson.annotations.SerializedName

data class Attr (

	@SerializedName("title") val title : String
)