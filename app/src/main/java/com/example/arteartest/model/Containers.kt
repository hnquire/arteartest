package com.example.arteartest.model

import com.google.gson.annotations.SerializedName

data class Containers (
	@SerializedName("attr") val attr : Attr,
	@SerializedName("items") val items : MutableList<Items>
)