package com.example.arteartest.model

import com.google.gson.annotations.SerializedName

data class Data(

    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("url") val url: String,
    @SerializedName("media") val media: Media
)