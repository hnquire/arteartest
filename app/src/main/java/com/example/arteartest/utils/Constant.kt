package com.example.arteartest.utils

class Constant {

    val baseURL = "https://s3.amazonaws.com/app-test-artear/"

    enum class articleType private constructor(val articleTypeNumber: Int) {
        category(0),
        article(1),
        webview(2),
    }


}