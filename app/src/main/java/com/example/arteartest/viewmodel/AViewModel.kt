package com.example.arteartest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.arteartest.model.ASections
import com.example.arteartest.network.sections.SectionsAPI
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class AViewModel : ViewModel() {

    val getSections = SectionsAPI()
    private val listSectionData = MutableLiveData<ASections>()
    private var listArticleData = MutableLiveData<ASections>()

    init {
        getSectionList()
    }

    fun setListSectionData(sectionList: ASections) {
        listSectionData.value = sectionList
    }

    fun getSectionList() {
            doAsync {
                getSections.getSections().subscribe({ sections ->

                    uiThread {
                        setListSectionData(sections)
                    }
                }) { Throwable ->
                    print(Throwable.toString())
                }
            }
        }

    fun getSectionListLiveData(): LiveData<ASections> {
        return listSectionData
    }

    fun getArticlesList(itemSelectedId: Int) {
        doAsync {
            getSections.getArticles(itemSelectedId).subscribe({ articles ->

                uiThread {
                    listArticleData.value = articles
                }
            }) { Throwable ->
                print(Throwable.toString())
            }
        }
    }

    fun getArticlesListLiveData(itemSelectedId: Int): LiveData<ASections> {
        listArticleData = MutableLiveData<ASections>()
        getArticlesList(itemSelectedId)
        return listArticleData
    }
}