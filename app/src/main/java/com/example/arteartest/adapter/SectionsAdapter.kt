package com.example.arteartest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.arteartest.R
import com.example.arteartest.model.Items
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.sections_cards.view.*

class SectionsAdapter(val sections: MutableList<Items>) :
    RecyclerView.Adapter<SectionsAdapter.SectionViewHolder>() {

    private var positionObservable = PublishSubject.create<Int>()
    var sectionObserver: Observable<Int> = positionObservable

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        return SectionViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.sections_cards, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        val section = sections[position]

        holder.sections_view.tv_title_section.text = section.data.title
        holder.sections_view.tv_description_section.text = section.data.description

        holder.sections_view.setOnClickListener {
            positionObservable.onNext(position)
        }
    }

    class SectionViewHolder(val sections_view: View) : RecyclerView.ViewHolder(sections_view)
}