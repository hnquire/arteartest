package com.example.arteartest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.arteartest.R
import com.example.arteartest.model.Items
import com.example.arteartest.model.Media
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.articles_cards.view.*

class ArticlesAdapter(val sections: MutableList<Items>) : RecyclerView.Adapter<ArticlesAdapter.ArticlesViewHolder>() {

    private var positionObservable = PublishSubject.create<Int>()
    var sectionObserver: Observable<Int> = positionObservable

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlesViewHolder {
        return ArticlesViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.articles_cards, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    override fun onBindViewHolder(holder: ArticlesViewHolder, position: Int) {
        val section = sections[position]

        holder.articles_view.tv_title_section.text = section.data.title
        holder.articles_view.tv_description_section.text = section.data.description
        if (section.data.media != null){
            holder.articles_view.iv_cover.visibility = (View.VISIBLE)
            val mMedia: Media = section.data.media
            val imageUrl = mMedia.data.url
            Picasso.get().load(imageUrl).placeholder(R.drawable.noimage).error(R.drawable.noimage).into(holder.articles_view.iv_cover)
        }else{
            holder.articles_view.iv_cover.visibility = (View.GONE)
        }

        holder.articles_view.setOnClickListener {
            positionObservable.onNext(position)
        }
    }

    class ArticlesViewHolder(val articles_view: View) : RecyclerView.ViewHolder(articles_view)
}