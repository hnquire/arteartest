package com.example.arteartest.ui

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.arteartest.R
import com.example.arteartest.adapter.ArticlesAdapter
import com.example.arteartest.adapter.SectionsAdapter
import com.example.arteartest.model.ASections
import com.example.arteartest.model.Containers
import com.example.arteartest.utils.Constant
import com.example.arteartest.viewmodel.AViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sectionsData: Containers
    private lateinit var viewModel: AViewModel
    private var articleType = Constant.articleType.category

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(AViewModel::class.java)

        getSectionData()
    }

    private fun getSectionData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Cargando...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val sectionsObserver = Observer<ASections> {
            sectionsData = it.containers[0]
            setAdapterSectionData()
            progressDialog.dismiss()
        }
        viewModel.getSectionListLiveData().observe(this, sectionsObserver)
    }

    private fun setAdapterSectionData() {
        rv_sections.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = SectionsAdapter(sectionsData.items)
        rv_sections.adapter = adapter

        adapter.sectionObserver.subscribe() { position ->
            var itemSelected = sectionsData.items[position]
            //Set Title
            tv_sections.text = itemSelected.data.title

            articleType = Constant.articleType.article
            getArticleData(itemSelected.data.id)
        }
    }


    private fun getArticleData(itemSelectedId: Int) {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Cargando...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val sectionsObserver = Observer<ASections> {
            if (it.containers != null) {
                setAdapterArticleData(it.containers[0])
                progressDialog.dismiss()
            } else {
                Toast.makeText(this, "Seccion Vacia: Seleccione otra Seccion", Toast.LENGTH_LONG).show()
                tv_sections.text = "Secciones"
                articleType = Constant.articleType.category
                getSectionData()
                progressDialog.dismiss()
            }
        }
        viewModel.getArticlesListLiveData(itemSelectedId).observe(this, sectionsObserver)
    }

    private fun setAdapterArticleData(articleData: Containers) {
        rv_sections.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = ArticlesAdapter(articleData.items)
        rv_sections.adapter = adapter

        adapter.sectionObserver.subscribe() { position ->
            var itemSelected = articleData.items[position]
            val url = itemSelected.data.url
            setWebView(url)
        }
    }

    fun setWebView(urlArticleSelected: String){
        var mywebview: WebView? = null
        mywebview = findViewById<WebView>(R.id.wv_article)
        mywebview.visibility = (View.VISIBLE)
        rv_sections.visibility = (View.GONE)

        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        mywebview!!.loadUrl(urlArticleSelected)
        articleType = Constant.articleType.webview
    }

    override fun onBackPressed() {
        if (articleType == Constant.articleType.article) {
            articleType = Constant.articleType.category
            rv_sections.visibility = (View.VISIBLE)
            tv_sections.text = "Secciones"

            getSectionData()
        } else {
            if (articleType == Constant.articleType.webview) {
                articleType = Constant.articleType.article
                rv_sections.visibility = (View.VISIBLE)
                wv_article.visibility = (View.GONE)
            } else {
                AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                    .setMessage("Are you sure?").setPositiveButton("yes",
                        DialogInterface.OnClickListener { dialog, which ->
                            val intent = Intent(Intent.ACTION_MAIN)
                            intent.addCategory(Intent.CATEGORY_HOME)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            finish()
                        }).setNegativeButton("no", null).show()
            }
        }
    }
}