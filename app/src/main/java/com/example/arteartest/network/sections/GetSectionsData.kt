package com.example.arteartest.network.sections

import com.example.arteartest.model.ASections
import com.example.arteartest.model.Containers
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface GetSectionsData {
    @get: GET("sections.json")
    val sections: Single<ASections>

    @GET
    fun getArticleData(@Url url: String): Single<ASections>
}