package com.example.arteartest.network.sections

import com.example.arteartest.model.ASections
import com.example.arteartest.network.RetrofitClient
import com.example.arteartest.utils.SectionsData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SectionsAPI {

    private var myCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getSections(): Single<ASections> {
        val retrofit = RetrofitClient.instance
        val requestInterface = retrofit.create(GetSectionsData::class.java)
        val sectionsRequest = requestInterface.sections
        myCompositeDisposable.add(
            sectionsRequest
                .retry(2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sectionsData ->
                    sectionsRequest.doOnSuccess { SectionsData.SectionSelected = sectionsData }
                }){ Throwable ->
                    print(Throwable.toString())
                }
        )
        return sectionsRequest
    }

    fun getArticles(itemSelectedId: Int): Single<ASections> {
        val retrofit = RetrofitClient.instance
        val requestInterface = retrofit.create(GetSectionsData::class.java)
        val sectionsRequest = requestInterface.getArticleData("section/$itemSelectedId/cover.json")
        myCompositeDisposable.add(
            sectionsRequest
                .retry(2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sectionsData ->
                    sectionsRequest.doOnSuccess { SectionsData.ArticleSelected = sectionsData }
                }){ Throwable ->
                    print(Throwable.toString())
                }
        )
        return sectionsRequest
    }
}